---
layout: handbook-page-toc
title: Channel SA Engagement Model
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

[**SA Practices**](/handbook/customer-success/solutions-architects/sa-practices/) - [**Sales Plays**](/handbook/customer-success/solutions-architects/sales-plays/) - [**Tools and Resources**](/handbook/customer-success/solutions-architects/tools-and-resources/) - [**Career Development**](/handbook/customer-success/solutions-architects/career-development/) - [**Demonstration**](/handbook/customer-success/solutions-architects/demonstrations/) - [**Processes**](/handbook/customer-success/solutions-architects/processes/)

## Channel Solutions Architecture Engagement Model

Engage a Channel SA when a GitLab Partner is involved in an opportunity and needs presales support, general enablement, or general practice building support.  We work closely with our CAMs who own the business aspects of the partner relationship, and foster relationships at an opportunity level between GitLab Account Managers and the Partner Account teams.

### How to Engage a Channel SA

The `#channel-solution-architects` slack channel is monitored by the CSA's globally.  If you do have a specific Partner related topic or question best handled by an SA, reach out to us there.

Regionally the Channel SA's coordinate issues within the following projects:

#### AMER

- https://gitlab.com/gitlab-com/channel/partners/amer/amer-internal

#### APAC

- https://gitlab.com/gitlab-com/channel/partners/apac/apac-internal

#### EMEA

- https://gitlab.com/gitlab-com/channel/partners/emea/emea-internal

